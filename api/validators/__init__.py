from api.validators.schemas import UserSchema

request_validator = UserSchema()

def validate_sign_up(data):
    try:
        request_validator.validate(data)
    except Exception as e:
        return e.args
