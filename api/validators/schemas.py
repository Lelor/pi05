from marshmallow import Schema, fields


class UserSchema(Schema):
    username = fields.Str(max_lenght=30)
    password = fields.Str(max_lenght=30)
    email = fields.Email()
    birthdate = fields.DateTime()
