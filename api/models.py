from datetime import datetime

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import (Column, String, DateTime, 
                        Text, Integer, ForeignKey)
from sqlalchemy.orm import relationship


db = SQLAlchemy()

class User(db.Model):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(String(30), nullable=False)
    password = Column(String(30), nullable=False)
    email = Column(String(50), nullable=False)
    birthdate = Column(DateTime(timezone=False), nullable=False)
    created_at = Column(DateTime(timezone=False),
                        default=datetime.now(),
                        nullable=False)


class Movie(db.Model):
    __tablename__ = 'movie'
    id = Column(Integer, primary_key=True)
    name = Column(String(40), nullable=False)
    description = Column(Text, nullable=False)
    pg = Column(Integer, nullable=False)
    gender = Column(String(20), nullable=False)


class MovieSession(db.Model):
    __tablename__ = 'movie_session'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime(timezone=False), nullable=False)
    idiome = Column(String(20), nullable=False)

    movie = Column(Integer(ForeignKey('movie.id')))
    movie_object = relationship('movie')

    def get_date(self):
        return datetime.strftime('%d/%m/%Y %H:%M')
